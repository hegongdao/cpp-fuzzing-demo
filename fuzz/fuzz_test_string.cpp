//
// Created by fuzzit.dev inc.
//

#include "test_string.h"

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

int FuzzAPI(const uint8_t * data, size_t size) {
      return size >= 3 &&
      data[0] == 'F' &&
      data[1] == 'U' &&
      data[2] == 'Z' &&
      data[3] == 'Z';
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t * data, size_t size) {
      return FuzzAPI(data, size);      
}
